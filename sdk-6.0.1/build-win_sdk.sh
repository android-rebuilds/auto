#!/bin/bash -ex
~/bin/repo init --quiet -u https://android.googlesource.com/platform/manifest -b android-6.0.1_r31  # r81 fails for win_sdk
~/bin/repo sync --quiet --current-branch -j4

# out/host/linux-x86/bin/jack-admin: line 27: USER: unbound variable
export USER=$(whoami)

. build/envsetup.sh
export BUILD_NUMBER='eng.6.0.1_r31'

# https://source.android.com/setup/build/building#choose-a-target
#lunch sdk-user  # fails for win_sdk
lunch sdk-eng
# winsdk-tools: also fails with r81
# https://android.googlesource.com/platform/sdk/+/master/docs/howto_build_SDK.txt
make win_sdk -j$(nproc) showcommands dist

ls -lh \
    out/host/windows/sdk/sdk/android-sdk_eng.6.0.1_r31_windows.zip
ls -lh \
    out/dist/android-sdk_eng.6.0.1_r31_windows.zip
