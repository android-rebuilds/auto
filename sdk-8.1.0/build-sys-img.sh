#!/bin/bash -ex
VERSION='8.1.0_r61'
BUILD_VARIANT='userdebug'
~/bin/repo init --quiet -u https://android.googlesource.com/platform/manifest -b android-$VERSION
~/bin/repo sync --quiet --current-branch -j4

export USER=$(whoami)

# https://android.googlesource.com/platform/sdk/+/master/docs/howto_build_SDK.txt
#export BUILD_NUMBER="${BUILD_VARIANT}.${VERSION}"
# work-around for error: ro.build.fingerprint cannot exceed 91 bytes: Android/sdk_arm64/generic_arm64:8.1.0/OPM8.190205.001/userdebug.8.1.0_r61:userdebug/test-keys (93)
export BUILD_NUMBER="ud.${VERSION}"
# https://source.android.com/setup/build/building#initialize
. build/envsetup.sh

make -j$(nproc) PRODUCT-sdk_arm64-sdk  showcommands dist sdk_repo
#make -j$(nproc) PRODUCT-sdk_x86_64-sdk showcommands dist sdk_repo
#make -j$(nproc) PRODUCT-sdk_x86_64-userdebug sdk showcommands dist sdk_repo
# => build/core/main.mk:436: error: The 'sdk' target may not be specified with any other targets.

ls -lh \
    out/host/linux-x86/sdk/sdk*/android-sdk_${BUILD_NUMBER}_linux-x86.zip \
    out/dist/android-sdk_${BUILD_NUMBER}_linux-x86.zip \
    out/dist/sdk-repo*.zip \
    out/dist/repo*.xml
