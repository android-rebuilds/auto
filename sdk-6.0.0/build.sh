#!/bin/bash -ex
~/bin/repo init --quiet -u https://android.googlesource.com/platform/manifest -b android-6.0.0_r1
~/bin/repo sync --quiet --current-branch -j4

export USER=$(whoami)
. build/envsetup.sh
lunch sdk-eng
make sdk -j$(nproc) showcommands
make win_sdk -j$(nproc) showcommands
