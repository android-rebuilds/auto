#!/bin/bash -ex
VERSION='11.0.0_r27'
BUILD_VARIANT='userdebug'
export USER=$(whoami)

# https://android.googlesource.com/platform/sdk/+/master/docs/howto_build_SDK.txt
export BUILD_NUMBER="${BUILD_VARIANT}.${VERSION}"
# https://source.android.com/setup/build/building?hl=en#initialize
. build/envsetup.sh
# https://source.android.com/setup/build/building?hl=en#choose-a-target
lunch sdk-${BUILD_VARIANT}

# System images (preferably with 'lunch sdk-userdebug')
# All these work, commented out for space reasons
m TARGET_PRODUCT=sdk         sdk  dist sdk_repo
#m TARGET_PRODUCT=sdk_arm64   sdk  dist sdk_repo
#m TARGET_PRODUCT=sdk_x86_64  sdk  dist sdk_repo
#m TARGET_PRODUCT=sdk_x86     sdk  dist sdk_repo

ls -lh \
    out/dist/sdk-repo*.zip \
    out/dist/repo*.xml
