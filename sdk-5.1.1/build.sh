#!/bin/bash -ex
~/bin/repo init --quiet -u https://android.googlesource.com/platform/manifest -b android-5.1.1_r37
~/bin/repo sync --quiet --current-branch -j4

export USER=$(whoami)
. build/envsetup.sh
lunch sdk-eng
make sdk -j$(nproc) showcommands
make win_sdk -j$(nproc) showcommands
ls -lh \
   out/host/linux-x86/sdk/sdk/android-sdk_eng.android_linux-x86.zip \
   out/host/windows/sdk/sdk/android-sdk_eng.android_windows.zip
