#!/bin/bash -ex

# https://android.googlesource.com/platform/tools/base/+/studio-master-dev/source.md
# As of 2021-01, studio-3.4.0 still generates tools-26.1.1, like studio-3.2.1
~/bin/repo init --quiet -u https://android.googlesource.com/platform/manifest -b studio-3.4.0

# 4.1 still broken as of 2021-01 AFAICS
# https://stackoverflow.com/questions/50946201/aosp-does-not-have-tools-vendor-google3-project/51058071
#~/bin/repo init --quiet -u https://android.googlesource.com/platform/manifest -b studio-4.1.1

~/bin/repo sync --quiet --current-branch -j4
