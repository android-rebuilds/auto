#!/bin/bash -ex
VERSION='2.2_r1'
#VERSION='2.2.3_r2.1'
~/bin/repo init --quiet -u https://android.googlesource.com/platform/manifest -b android-$VERSION
~/bin/repo sync --quiet --current-branch -j4

rm -rf prebuilt/sdk/{5,6,7,8,tools}/

# $ grep -r 'LOCAL_SDK_VERSION := ' . | grep -v current$
# ./packages/experimental/BugReportSender/Android.mk:LOCAL_SDK_VERSION := 4
rm -f prebuilt/sdk/4/android.jar
curl https://android-rebuilds.beuc.net/dl/prebuilts/sdk/4/android.jar \
  > prebuilt/sdk/4/android.jar
rm -f prebuilt/sdk/4/framework.aidl
curl https://android-rebuilds.beuc.net/dl/prebuilts/sdk/4/framework.aidl \
  > prebuilt/sdk/4/framework.aidl
